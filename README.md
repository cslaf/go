# Go CI/CD Component

> 🚧 **NOTE** 🚧
>
> This component is [being migrated from CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/issues/437104) and **work-in-progress**, where inputs, template names, and interfaces might change through 0.x.y releases.
>
> Please wait for 1.x.y releases for production usage.

This CI/CD component provides job templates to format, build and test Go source code. You can specify the Go version as input, which generates dynamic job names. This allows multiple includes of the same component, to test source code compatibility with different Go versions.

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml` file by using the `include:`
keyword, where `<VERSION>` is the latest released tag or `main`.

### Individual job templates

Individual jobs are available as separate component templates: `format`, `build`, `test`.

```yaml
include:
  # format
  - component: gitlab.com/components/go/format@<VERSION>
    inputs:
      stage: format 
      go_version: latest
  # build
  - component: gitlab.com/components/go/build@<VERSION>
    inputs:
      stage: build
      go_version: latest   
  # test 
  - component: gitlab.com/components/go/test@<VERSION>
    inputs:
      stage: test
      go_version: latest   
```

The job names automatically get the `go_version` value added as string suffix, for example: `build-latest` or `build-1.21.6`.

#### Inputs

##### Format

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `format` | The format stage name |
| `go_version` | `latest` | The Go image tag version from https://hub.docker.com/_/Go |

##### Build

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The build stage name |
| `go_version` | `latest` | The Go image tag version from https://hub.docker.com/_/Go |
| `binary_directory` | `mybinaries` | The binary output directory, saved as build artifacts. |

##### Test

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The test stage name |
| `go_version` | `latest` | The Go image tag version from https://hub.docker.com/_/Go |

### Full pipeline

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/go/full-pipeline@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

#### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage_format` | `format` | The format stage name |
| `stage_test` | `test` | The test stage name |
| `stage_build` | `build` | The build stage name |
| `go_version` | `latest` | The Go image tag version |
| `binary_directory` | `mybinaries` | The binary output directory, saved as build artifacts. |

## Contributing

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 

Author: Michael Friedrich, @dnsmichi